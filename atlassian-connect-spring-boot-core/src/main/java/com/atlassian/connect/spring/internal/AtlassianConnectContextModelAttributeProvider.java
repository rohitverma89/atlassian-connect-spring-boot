package com.atlassian.connect.spring.internal;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.auth.AtlassianConnectSecurityContextHelper;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

/**
 * A controller utility class that maps the standard Atlassian Connect iframe context parameters to Spring model
 * attributes.
 */
@ControllerAdvice
public class AtlassianConnectContextModelAttributeProvider {

    private static final String ALL_JS_FILENAME = "all.js";

    private static final String ALL_DEBUG_JS_FILENAME = "all-debug.js";

    private HttpServletRequest request;

    private AtlassianConnectProperties atlassianConnectProperties;

    private AtlassianConnectSecurityContextHelper securityContextHelper;

    private SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator;

    public AtlassianConnectContextModelAttributeProvider(HttpServletRequest request,
            AtlassianConnectProperties atlassianConnectProperties,
            AtlassianConnectSecurityContextHelper securityContextHelper,
            SelfAuthenticationTokenGenerator selfAuthenticationTokenGenerator) {
        this.request = request;
        this.atlassianConnectProperties = atlassianConnectProperties;
        this.securityContextHelper = securityContextHelper;
        this.selfAuthenticationTokenGenerator = selfAuthenticationTokenGenerator;
    }

    @ModelAttribute
    public void addAttributes(Model model) {
        addModelAttribute(model, "atlassianConnectLicense", getLicense());
        addModelAttribute(model, "atlassianConnectLocale", getLocale());
        addModelAttribute(model, "atlassianConnectTimezone", getTimezone());
        addModelAttribute(model, "atlassianConnectAllJsUrl", getAllJsUrl());
        addModelAttribute(model, "atlassianConnectToken", getSelfAuthenticationToken());
    }

    public String getLicense() {
        return request.getParameter("lic");
    }

    public String getLocale() {
        return request.getParameter("loc");
    }

    public String getTimezone() {
        return request.getParameter("tz");
    }

    public String getAllJsUrl() {
        return getHostBaseUrl().map(this::createAllJsUrl).orElse("");
    }

    public String getSelfAuthenticationToken() {
        return securityContextHelper.getHostUserFromSecurityContext()
                .map(selfAuthenticationTokenGenerator::createSelfAuthenticationToken)
                .orElse(null);
    }

    private void addModelAttribute(Model model, String attributeName, String value) {
        String dashedAttributeName = String.join("-", attributeName.split("(?=\\p{Upper})")).toLowerCase();
        model.addAttribute(attributeName, value);
        model.addAttribute(dashedAttributeName, value);
    }

    private Optional<String> getHostBaseUrl() {
        Optional<String> optionalBaseUrl = securityContextHelper.getHostFromSecurityContext().map(AtlassianHost::getBaseUrl);
        if (!optionalBaseUrl.isPresent()) {
            optionalBaseUrl = getHostBaseUrlFromQueryParameters();
        }
        return optionalBaseUrl;
    }

    private Optional<String> getHostBaseUrlFromQueryParameters() {
        String hostUrl = request.getParameter("xdm_e");
        String contextPath = request.getParameter("cp");

        Optional<String> optionalBaseUrl = Optional.empty();
        if (!StringUtils.isEmpty(hostUrl)) {
            if (!StringUtils.isEmpty(contextPath)) {
                optionalBaseUrl = Optional.of(hostUrl + contextPath);
            } else {
                optionalBaseUrl = Optional.of(hostUrl);
            }
        }
        return optionalBaseUrl;
    }

    private String createAllJsUrl(String hostBaseUrl) {
        return String.format("%s/%s/%s", hostBaseUrl, "atlassian-connect", getAllJsFilename());
    }

    private String getAllJsFilename() {
        return atlassianConnectProperties.isDebugAllJs() ? ALL_DEBUG_JS_FILENAME : ALL_JS_FILENAME;
    }
}
