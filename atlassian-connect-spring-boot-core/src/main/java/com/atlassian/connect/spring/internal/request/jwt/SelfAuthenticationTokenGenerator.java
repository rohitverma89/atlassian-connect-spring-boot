package com.atlassian.connect.spring.internal.request.jwt;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.internal.AtlassianConnectProperties;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

/**
 * A generator of JSON Web Tokens for authenticating requests from the add-on to itself.
 */
@Component
public class SelfAuthenticationTokenGenerator {

    /**
     * The name of the JWT claim used for the client key of the Atlassian host in self-authentication tokens
     */
    public static final String HOST_CLIENT_KEY_CLAIM = "clientKey";

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Autowired
    private AtlassianConnectProperties atlassianConnectProperties;

    public String createSelfAuthenticationToken(AtlassianHostUser hostUser) {
        Duration expirationTime = Duration.of(atlassianConnectProperties.getSelfAuthenticationExpirationTime(), ChronoUnit.SECONDS);
        JwtBuilder jwtBuilder = new JwtBuilder(expirationTime)
                .issuer(addonDescriptorLoader.getDescriptor().getKey())
                .audience(addonDescriptorLoader.getDescriptor().getKey())
                .claim(HOST_CLIENT_KEY_CLAIM, hostUser.getHost().getClientKey())
                .signature(hostUser.getHost().getSharedSecret());
        hostUser.getUserKey().ifPresent(jwtBuilder::subject);
        return jwtBuilder.build();
    }
}
