package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.internal.request.AtlassianConnectHttpRequestInterceptor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;

import java.util.Optional;

public class FixedJwtSigningClientHttpRequestInterceptor extends AtlassianConnectHttpRequestInterceptor {

    private String jwt;

    public FixedJwtSigningClientHttpRequestInterceptor(String jwt) {
        super("some-version");
        this.jwt = jwt;
    }

    @Override
    protected Optional<AtlassianHost> getHostForRequest(HttpRequest request) {
        AtlassianHost host = new AtlassianHost();
        host.setBaseUrl(AtlassianHosts.BASE_URL);
        return Optional.of(host);
    }

    @Override
    protected HttpRequest rewrapRequest(HttpRequest request, AtlassianHost host) {
        request.getHeaders().set(HttpHeaders.AUTHORIZATION, String.format("JWT %s", jwt));
        return request;
    }
}
