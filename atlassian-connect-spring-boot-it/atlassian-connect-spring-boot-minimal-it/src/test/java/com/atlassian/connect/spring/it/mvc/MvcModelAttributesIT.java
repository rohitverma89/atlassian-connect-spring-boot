package com.atlassian.connect.spring.it.mvc;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.IgnoreJwt;
import com.atlassian.connect.spring.internal.descriptor.AddonDescriptorLoader;
import com.atlassian.connect.spring.internal.jwt.JwtParseException;
import com.atlassian.connect.spring.internal.jwt.JwtReader;
import com.atlassian.connect.spring.internal.jwt.JwtVerificationException;
import com.atlassian.connect.spring.internal.request.jwt.SelfAuthenticationTokenGenerator;
import com.atlassian.connect.spring.it.util.BaseApplicationIT;
import com.nimbusds.jwt.JWTClaimsSet;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Collections;
import java.util.Optional;

import static com.atlassian.connect.spring.it.util.AtlassianHosts.createAndSaveHost;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MvcModelAttributesIT extends BaseApplicationIT {

    @Autowired
    private AddonDescriptorLoader addonDescriptorLoader;

    @Test
    public void shouldReturnAllJsModelAttributeForAuthenticated() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        setJwtAuthenticatedPrincipal(host);
        String expectedUrl = String.format("%s/atlassian-connect/all-debug.js", host.getBaseUrl());
        mvc.perform(get("/model")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectAllJsUrl']").value(is(expectedUrl)))
                .andExpect(jsonPath("$.['atlassian-connect-all-js-url']").value(is(expectedUrl)));
    }

    @Test
    public void shouldReturnAllJsModelAttributeForAnonymous() throws Exception {
        mvc.perform(get("/model-public")
                .param("xdm_e", "http://some-host.com")
                .param("cp", "/context-path")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectAllJsUrl']").value(is("http://some-host.com/context-path/atlassian-connect/all-debug.js")))
                .andExpect(jsonPath("$.['atlassian-connect-all-js-url']").value(is("http://some-host.com/context-path/atlassian-connect/all-debug.js")));
    }

    @Test
    public void shouldReturnLicenseModelAttribute() throws Exception {
        String license = "none";
        mvc.perform(get("/model-public")
                .param("lic", license)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectLicense']").value(is(license)))
                .andExpect(jsonPath("$.['atlassian-connect-license']").value(is(license)));
    }

    @Test
    public void shouldReturnLocaleModelAttribute() throws Exception {
        String locale = "en-GB";
        mvc.perform(get("/model-public")
                .param("loc", locale)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectLocale']").value(is(locale)))
                .andExpect(jsonPath("$.['atlassian-connect-locale']").value(is(locale)));
    }

    @Test
    public void shouldReturnTimezoneModelAttribute() throws Exception {
        String timezone = "Australia/Sydney";
        mvc.perform(get("/model-public")
                .param("tz", timezone)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectTimezone']").value(is(timezone)))
                .andExpect(jsonPath("$.['atlassian-connect-timezone']").value(is(timezone)));
    }

    @Test
    public void shouldReturnPageJwtTokenModelAttributeForAuthenticated() throws Exception {
        AtlassianHost host = createAndSaveHost(hostRepository);
        String subject = "charlie";
        setJwtAuthenticatedPrincipal(host, subject);

        String addonKey = addonDescriptorLoader.getDescriptor().getKey();
        mvc.perform(get("/model")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectToken']").value(allOf(
                        validJwtWithClaim(host, "iss", addonKey),
                        validJwtWithClaim(host, "aud", Collections.singletonList(addonKey)),
                        validJwtWithClaim(host, "sub", subject),
                        validJwtWithClaim(host, SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM, host.getClientKey()))))
                .andExpect(jsonPath("$.['atlassian-connect-token']").value(allOf(
                        validJwtWithClaim(host, "iss", addonKey),
                        validJwtWithClaim(host, "aud", Collections.singletonList(addonKey)),
                        validJwtWithClaim(host, "sub", subject),
                        validJwtWithClaim(host, SelfAuthenticationTokenGenerator.HOST_CLIENT_KEY_CLAIM, host.getClientKey()))));
    }

    @Test
    public void shouldNotReturnPageJwtTokenModelAttributeForAnonymous() throws Exception {
        mvc.perform(get("/model-public")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.['atlassianConnectToken']").value(nullValue()))
                .andExpect(jsonPath("$.['atlassian-connect-token']").value(nullValue()));
    }

    private Matcher<String> validJwtWithClaim(final AtlassianHost host, final String claimName, final Object expected) {
        return new TypeSafeMatcher<String>() {

            @Override
            protected boolean matchesSafely(String token) {
                return parseJwt(token).map(this::hasClaimValue).orElse(false);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText(String.format("JWT having \"%s\" claim with value \"%s\"", claimName, expected));
            }

            @Override
            protected void describeMismatchSafely(String item, Description mismatchDescription) {
                String itemToDescribe = parseJwt(item).map(JWTClaimsSet::toString).orElse(String.format("Invalid JWT: %s", item));
                super.describeMismatchSafely(itemToDescribe, mismatchDescription);
            }

            private boolean hasClaimValue(JWTClaimsSet claims) {
                return Optional.ofNullable(claims.getClaim(claimName)).map((claimValue) -> claimValue.equals(expected)).orElse(false);
            }

            private Optional<JWTClaimsSet> parseJwt(String token) {
                try {
                    return Optional.of(new JwtReader(host.getSharedSecret()).readAndVerify(token, null));
                } catch (JwtParseException e) {
                    return Optional.empty();
                } catch (JwtVerificationException e) {
                    return Optional.empty();
                }
            }
        };
    }

    @TestConfiguration
    public static class ModelAttributesControllerConfiguration {

        @Bean
        public ModelAttributesController modelAttributesController() {
            return new ModelAttributesController();
        }
    }

    @Controller
    public static class ModelAttributesController {

        @IgnoreJwt
        @GetMapping(value = "/model-public")
        public ModelAndView getAnonymousModelAttributes(Model model) {
            return new ModelAndView(new MappingJackson2JsonView(), model.asMap());
        }

        @GetMapping(value = "/model")
        public ModelAndView getModelAttributes(Model model) {
            return getAnonymousModelAttributes(model);
        }
    }
}
